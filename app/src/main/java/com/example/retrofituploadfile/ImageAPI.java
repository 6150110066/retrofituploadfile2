package com.example.retrofituploadfile;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;


public interface ImageAPI {
    public static final String BASE_URL = "http://10.0.2.2/retrofit/";
    @GET("list_images.php")
    Call<List<ImagesList>> getImages();
}